<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('stats_prescription_reactions', function (Blueprint $table) {
            $table->unsignedBigInteger('stat_id');
            $table->unsignedBigInteger('prescription_reaction_id');

            $table->foreign('stat_id')->references('id')->on('stats')->onDelete('cascade');
            $table->foreign('prescription_reaction_id')->references('id')->on('prescription_reactions')->onDelete('cascade');

            $table->primary(['stat_id', 'prescription_reaction_id']);});
    }

    public function down()
    {
        Schema::dropIfExists('stats_prescription_reactions');
    }

};

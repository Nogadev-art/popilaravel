<?php

namespace App\Http\Controllers;

use App\Http\Requests\Stats\StoreStatsItemRequest;
use App\Models\PrescriptionReaction;
use App\Models\Stat;
use App\Enums\EnumChoice as Choice;
use App\Models\StatItem;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class StatController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'userId' => 'required|string',
            'userAge' => 'required|integer',
            'userGender' => 'required|string',
            'userInstitution' => 'required|string',
            'userProfession' => 'required|integer',
            'timeSpent' => 'required|integer',
            'patientAge' => 'required|integer',
            'patientGender' => 'required|integer',
            'prescriptionReactions' => 'required|array',
            'prescriptionReactions.*.id' => 'required|integer|exists:prescriptions,id', // Assurez-vous que cet ID existe bien dans la table `prescription_reactions`.
            'prescriptionReactions.*.reaction' => ['required', 'integer', Rule::in(Choice::cases())],
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $data = $validator->validated();
        $stat = new Stat();

        $stat->user_id = $data['userId'];
        $stat->user_age = $data['userAge'];
        $stat->user_gender = $data['userGender'];
        $stat->user_institution = $data['userInstitution'];
        $stat->user_profession = $data['userProfession'];
        $stat->time_spent = $data['timeSpent'];
        $stat->patient_age = $data['patientAge'];
        $stat->patient_gender = $data['patientGender'];
        $stat->save();

        foreach ($data['prescriptionReactions'] as $reactionData) {
            $prescriptionReactions = new PrescriptionReaction();
            $prescriptionReactions->prescription_id = $reactionData['id'];
            $prescriptionReactions->choice = $reactionData['reaction'];
            $prescriptionReactions->save();
            $stat->prescriptionReactions()->attach($prescriptionReactions);
        }


        return response()->json(['prescriptionReactions' => $data['prescriptionReactions']], 201);
    }



    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
     {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatItem extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $table = 'stats_prescription_reactions';
    protected $fillable = ['stats_id', 'prescription_reactions_id'];
}

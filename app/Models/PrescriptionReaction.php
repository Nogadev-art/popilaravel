<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrescriptionReaction extends Model
{
    use HasFactory;
    protected $fillable = ['choice', 'prescription_id'];


    public $timestamps = false;

    public function stats()
    {
        return $this->belongsToMany(Stat::class,'stats_prescription_reactions');
    }
}

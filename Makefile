help:
	@echo "Utilisation:"
	@echo "  make release MSG='Votre message de commit'"
	@echo "  make deploy : Déploie le code sur le serveur en FTP"
	@echo "  make install : Installe le code sur le serveur key, cache, optimize"

release: commit deploy install

commit:
	@if [ -z "$(MSG)" ]; then \
		echo "Erreur: MSG est vide. Utilisation: make release MSG='votre message'"; \
		false; \
	else \
		git add . && \
		git commit -m "$(MSG)" && \
		echo "Commit effectué avec le message: $(MSG)"; \
	fi

deploy:
	@echo "Déploiement en cours..."
	ssh weblink 'cd  ~/sites/popi-back.nogadev.ch/popilaravel && git pull origin main'


install:
	@echo "Installation en cours..."
	ssh weblink ' cd  ~/sites/popi-back.nogadev.ch/popilaravel && php artisan key:generate && php artisan config:cache && php artisan cache:clear && php artisan optimize'

